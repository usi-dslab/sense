/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPDestination;
import ch.usi.dslab.bezerra.netwrapper.tcp.TCPSender;
import ch.usi.dslab.bezerra.sense.datapoints.DataPoint;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


public class DataPointList implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(DataPointList.class);

    public static void sendToGatherer(DataPointList dpl) {
        sendToGatherer(dpl, true);
    }

    public static void sendToGatherer(DataPointList dpl, boolean compress) {
        TCPSender tcpSender = new TCPSender();
        TCPDestination gathererDestination = DataGatherer.getGathererDestination();
        Message msgToGatherer = null;
        long tstart = System.nanoTime();
        int numBytes = 0;
        logger.info(String.format("DataPointList of %s being serialized...", dpl.getLogType().getValue0()));
        if (compress) {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                GZIPOutputStream gzipOut = new GZIPOutputStream(baos);
                ObjectOutputStream objectOut = new ObjectOutputStream(gzipOut);
                objectOut.writeObject(dpl);
                objectOut.close();
                byte[] bytes = baos.toByteArray();
                numBytes = bytes.length;
//            logger.info(String.format("DataPointList of %s has %d bytes compressed", dpl.getLogType().getValue0(), bytes.length));
                msgToGatherer = new Message(true, bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            msgToGatherer = new Message(false, dpl);
        }
        long tend = System.nanoTime();
        double tseconds = (tend - tstart) / 1e9;
        logger.info(String.format("Serialization to %d bytes took %.3f seconds", numBytes, tseconds));
        tcpSender.send(msgToGatherer, gathererDestination);
    }

    public static DataPointList receiveAtGatherer(Message msg) {
        long tstart = System.nanoTime();
        logger.info("DataGatherer received log. Deserializing... ");
        DataPointList dpl = null;
        boolean compressed = (Boolean) msg.getItem(0);
        if (compressed) {
//         Codec lz4 = new CodecLZ4();
//         dpl = (DataPointList) lz4.createObjectFromBytes((byte[]) msg.getItem(1));

            try {
                byte[] bytes = (byte[]) msg.getItem(1);
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                GZIPInputStream gzipIn = new GZIPInputStream(bais);
                ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
                dpl = (DataPointList) objectIn.readObject();
                objectIn.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            dpl = (DataPointList) msg.getItem(1);
        }
        long tend = System.nanoTime();
        logger.info(String.format("Deserialization took %.3f seconds", (tend - tstart) / 1e9));
        return dpl;
    }

    String monitoredResource;
    String monitoredType;
    int monitoredId;
    boolean recquired;

    List<DataPoint> dataPointList;

    public DataPointList() {
    }

    public DataPointList(String monitoredResource, String monitoredType, int monitoredId, boolean recquired, List<DataPoint> dataPointList) {
        this.monitoredResource = monitoredResource;
        this.monitoredType = monitoredType;
        this.monitoredId = monitoredId;
        this.recquired = recquired;
        this.dataPointList = dataPointList;
    }

    public List<DataPoint> getList() {
        return dataPointList;
    }

    public boolean isEmpty() {
        return dataPointList.isEmpty();
    }

    public void abortProcessIfEmpty() {
        if (isEmpty()) {
            System.err.println(String.format("ERROR: DataGatherer received an empty DataPointList of %s from %s %d", monitoredResource, monitoredType, monitoredId));
            System.exit(1);
        }
    }

    public DataPoint getOne() {
        if (dataPointList.size() > 0)
            return dataPointList.get(0);
        else {
            System.err.println(String.format("DataPoint.getOne() of empty list with monitoredResource:%s, monitoredType:%s, monitoredId:%d, recquired:%b",
                    monitoredResource, monitoredType, monitoredId, recquired));
            return null;
        }
    }

    public Pair<String, String> getLogType() {
        return new Pair<String, String>(this.monitoredResource, this.monitoredType);
    }

    public int getMonitoredId() {
        return monitoredId;
    }

    public DataPoint getAverage() {
        return getOne().getAverage(getList());
    }

    public DataPoint getAggregate() {
        return getOne().getAggregate(getList());
    }

    public long getFirstTime() {
        return dataPointList.get(0).getInstant();
    }

    public long getLastTime() {
        return dataPointList.get(dataPointList.size() - 1).getInstant();
    }

    public boolean addDataPoint(DataPoint dataPoint) {
        return dataPointList.add(dataPoint);
    }

    public void appendList(DataPointList another) {
        dataPointList.addAll(another.dataPointList);
    }
}
