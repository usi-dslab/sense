/*
 * Nest - A library for developing DSSMR-based services
 * Copyright (C) 2015, University of Lugano
 *
 *  This file is part of Nest.
 *
 *  Nest is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import ch.usi.dslab.bezerra.sense.datapoints.CustomTextDataPoint;

import java.util.Arrays;
import java.util.List;

public class CustomTextPassiveMonitor extends PassiveMonitor {

    String currentMessage;
    String delimiter;

    public CustomTextPassiveMonitor(int monitoredPid, String monitoredName) {
        super(monitoredPid, monitoredName);
    }

    public CustomTextPassiveMonitor(int monitoredPid, String monitoredName, boolean recquired, String delimiter) {
        super(monitoredPid, monitoredName, recquired);
        this.delimiter = delimiter;
    }

    public void logMessage(String... obj) {
        if (active == false)
            return;
        List<String> list = Arrays.asList(obj);
        currentMessage = String.join(delimiter, list);
        log();
    }

    synchronized void log() {
        if (active == false) {
            return;
        }
        long now = System.currentTimeMillis();
        log.add(new CustomTextDataPoint(lastLogTime, now, currentMessage));
        lastLogTime = now;


    }

    @Override
    public void saveToFile() {
        saveToFile(new CustomTextDataPoint());
    }

    @Override
    public void sendToGatherer() {
        sendToGatherer(new CustomTextDataPoint());
    }
}
