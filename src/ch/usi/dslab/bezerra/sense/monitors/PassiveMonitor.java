/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.DataPointList;
import ch.usi.dslab.bezerra.sense.datapoints.DataPoint;

public abstract class PassiveMonitor {
   
   public static class Storer extends Thread {
      PassiveMonitor parentMonitor;
      
      public Storer(PassiveMonitor parentMonitor) {
         this.parentMonitor = parentMonitor;
      }
      
      @Override
      public void run() {
         // sleep for the set duration of the experiment
         try {
            Thread.sleep(DataGatherer.getDuration());
         } catch (InterruptedException e) {
            System.err.println("Failed to sleep for duration of the experiment.");
            e.printStackTrace();
            System.exit(1);
         }
         
         parentMonitor.stopLogging();
         
         if (DataGatherer.isSavingToDisk())
            parentMonitor.saveToFile();
         if (DataGatherer.isSendingToGatherer())
            parentMonitor.sendToGatherer();
      }
   }
   
   long lastLogTime = System.currentTimeMillis();
   List<DataPoint> log = new ArrayList<DataPoint>();

   String  logFilename;
   String  gathererAddress;
   int     gathererPort;
   Storer  storer;
   int     monitoredPid;
   String  monitoredName;
   boolean active = true;
   boolean recquired;

   public PassiveMonitor(int monitoredPid, String monitoredName) {
      this(monitoredPid, monitoredName, true);
   }
   
   public PassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      this.monitoredPid  = monitoredPid;
      this.monitoredName = monitoredName;
      this.recquired = recquired;
      storer = new Storer(this);
      storer.start();
   }
   
   synchronized public void stopLogging() {
      active = false;
   }

   protected void saveToFile(DataPoint dptype) {
      Path logPath = Paths.get(DataGatherer.getLogDirectory(), dptype.getName() + "_" + monitoredName + "_" + monitoredPid + ".log");
      try {
         BufferedWriter writer = Files.newBufferedWriter(logPath, StandardCharsets.UTF_8);
         writer.write("# " + dptype.getLogHeader());
         writer.newLine();
         for (DataPoint dp : log) {
            writer.write(dp.toString());
            writer.newLine();
         }
         writer.close();
      }
      catch (IOException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }

   protected void sendToGatherer(DataPoint dptype) {
      DataPointList dpList = new DataPointList(dptype.getName(), monitoredName, monitoredPid, recquired, log);
      DataPointList.sendToGatherer(dpList);
   }
   
   abstract public void saveToFile();
   abstract public void sendToGatherer();
}
