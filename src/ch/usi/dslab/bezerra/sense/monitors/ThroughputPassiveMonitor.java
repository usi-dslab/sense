/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import java.util.concurrent.atomic.AtomicLong;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.datapoints.ThroughputDataPoint;

public class ThroughputPassiveMonitor extends PassiveMonitor {

   public ThroughputPassiveMonitor(int monitoredPid, String monitoredName) {
      super(monitoredPid, monitoredName);
   }

   public ThroughputPassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      super(monitoredPid, monitoredName, recquired);
   }

   AtomicLong currentCommandCount = new AtomicLong();
   AtomicLong previousCommandCount = new AtomicLong();

   public void incrementCount() {
      if (active == false)
         return;
      incrementCount(1);
   }

   public void incrementCount(int value) {
      if (active == false) {
         return;
      }
      currentCommandCount.addAndGet(value);
      log();
   }

   public long getCount() {
      return currentCommandCount.get();
   }

   synchronized void log() {
      if (active == false) {
         return;
      }
      long now = System.currentTimeMillis();
      if (now > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
         double throughput = (currentCommandCount.get() - previousCommandCount.get()) / ((now - lastLogTime) / 1000d);
         log.add(new ThroughputDataPoint(lastLogTime, now, throughput));
         lastLogTime = now;
         previousCommandCount.set(currentCommandCount.get());
      }
   }

   @Override
   public void saveToFile() {
      saveToFile(new ThroughputDataPoint());
   }

   @Override
   public void sendToGatherer() {
      sendToGatherer(new ThroughputDataPoint());
   }
}
