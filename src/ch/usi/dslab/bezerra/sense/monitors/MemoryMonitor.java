/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Long Hoang Le - long.hoang.le@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.DataPointList;
import ch.usi.dslab.bezerra.sense.datapoints.DataPoint;
import ch.usi.dslab.bezerra.sense.datapoints.MemoryDataPoint;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MemoryMonitor {

    static String delims = " +";
    public static final int logInterval = 1000; // milliseconds
    long durationMS;

    List<DataPoint> memoryLog;

    String monitoredName;
    int monitoredId;

    public MemoryMonitor(int monitoredId, String monitoredName) {
        this.monitoredId = monitoredId;
        this.monitoredName = monitoredName;
        this.durationMS = DataGatherer.getDuration();
        this.memoryLog = new ArrayList<DataPoint>();
    }

    // returns user cpu usage
    public void logMemory() {
        try {
            // start up the command in child process
            String cmd = "free -m |grep Mem";
            Process child = Runtime.getRuntime().exec(cmd);

            // hook up child process output to parent
            InputStream lsOut = child.getInputStream();
            InputStreamReader r = new InputStreamReader(lsOut);
            BufferedReader in = new BufferedReader(r);

            // read the child process' output
            String line = in.readLine();
            line = in.readLine();

//            System.out.println(line);
            MemoryDataPoint mem = new MemoryDataPoint(System.currentTimeMillis());
            line = line.substring(line.indexOf(":") + 1);
            line = line.trim();
            String[] values = line.split(" +");
//            for (String str : values) System.out.print(str+" ");
//            System.out.println();
            long total = Long.parseLong(values[0]);
            long used = Long.parseLong(values[1]);
            long free = Long.parseLong(values[2]);
            long shared = Long.parseLong(values[3]);
            long cache = Long.parseLong(values[4]);
            long available = Long.parseLong(values[5]);

            mem.appendMemoryUsage(total, used, free, shared, cache, available);
//            System.out.println("USAGE:" + mem);
            in.close();

            memoryLog.add(mem);
        } catch (IOException e) { // exception thrown
            e.printStackTrace();
            System.out.println("!!! Error when getting memory load.");
            System.exit(1);
        }
    }

    public void log() {
        long startMS, currentMS;
        startMS = currentMS = System.currentTimeMillis();

        while (currentMS - startMS < durationMS) {
            logMemory();
            try {
                Thread.sleep(logInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.err.println("!!! Failed while putting thread to sleep");
                System.exit(1);
            }
            currentMS = System.currentTimeMillis();
        }
    }

    void sendToGatherer() {
        DataPointList dpList = new DataPointList((new MemoryDataPoint()).getName(), monitoredName, monitoredId, true, memoryLog);
        DataPointList.sendToGatherer(dpList);

    }

    public static void main(String[] args) {

        if (args.length != 6) {
            System.out.println("1usage: nodeTypeName nodeId gathererIp gathererPort duration(s) logDirectory");
            System.exit(1);
        }

        String nodeType = args[0];
        int id = Integer.parseInt(args[1]);
        String gaddr = args[2];
        int gport = Integer.parseInt(args[3]);
        long durationSecs = Long.parseLong(args[4]);
        String logDirectory = args[5];

        DataGatherer.configure(durationSecs, logDirectory, gaddr, gport);

        MemoryMonitor cm = new MemoryMonitor(id, nodeType);

        cm.log();
        cm.saveToFile(new MemoryDataPoint());
        cm.sendToGatherer();
    }

    protected void saveToFile(DataPoint dptype) {
        Path logPath = Paths.get(DataGatherer.getLogDirectory(), dptype.getName() + "_" + monitoredName + "_" + monitoredId + ".log");
        try {
            BufferedWriter writer = Files.newBufferedWriter(logPath, StandardCharsets.UTF_8);
            writer.write("# " + dptype.getLogHeader());
            writer.newLine();
            for (DataPoint dp : memoryLog) {
                writer.write(dp.toString());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }


}
