/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.DataPointList;
import ch.usi.dslab.bezerra.sense.datapoints.CPUDataPoint;
import ch.usi.dslab.bezerra.sense.datapoints.DataPoint;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CPUMonitor {

    static String delims = " +";
    public static final int logInterval = 500; // milliseconds
    long durationMS;

    List<DataPoint> cpuLog;

    String monitoredName;
    int monitoredId;

    public CPUMonitor(int monitoredId, String monitoredName) {
        this.monitoredId = monitoredId;
        this.monitoredName = monitoredName;
        this.durationMS = DataGatherer.getDuration();
        this.cpuLog = new ArrayList<DataPoint>();
    }

    // returns user cpu usage
    public void logCpu() {
        try {
            // start up the command in child process
            String cmd = "mpstat -P ALL 1 1";
            Process child = Runtime.getRuntime().exec(cmd);

            // hook up child process output to parent
            InputStream lsOut = child.getInputStream();
            InputStreamReader r = new InputStreamReader(lsOut);
            BufferedReader in = new BufferedReader(r);

            // read the child process' output
            String line = in.readLine();
            if (line != null)
                line = in.readLine();
            if (line != null)
                line = in.readLine();
            if (line != null)
                line = in.readLine();
//         System.out.println(line);
            CPUDataPoint clp = new CPUDataPoint(System.currentTimeMillis());
            while (line != null && !line.equals("")) {
                String[] columns = line.split(delims);
                double user = Double.parseDouble(columns[3]);
                double sys = Double.parseDouble(columns[5]);
                double idle = Double.parseDouble(columns[11]);
                double load = user + sys;
                line = in.readLine();

                clp.appendCoreLoad(load, user, sys, idle);
            }

            in.close();

            cpuLog.add(clp);
        } catch (IOException e) { // exception thrown
            e.printStackTrace();
            System.out.println("!!! Error when getting cpu (and cores) load.");
            System.exit(1);
        }
    }

    public void log() {
        long startMS, currentMS;
        startMS = currentMS = System.currentTimeMillis();

        while (currentMS - startMS < durationMS) {
            logCpu();
            try {
                Thread.sleep(logInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.err.println("!!! Failed while putting thread to sleep");
                System.exit(1);
            }
            currentMS = System.currentTimeMillis();
        }
    }

    void sendToGatherer() {
        DataPointList dpList = new DataPointList((new CPUDataPoint()).getName(), monitoredName, monitoredId, true, cpuLog);
        DataPointList.sendToGatherer(dpList);

    }

    public static void main(String[] args) {

        if (args.length != 6) {
            System.out.println("1usage: nodeTypeName nodeId gathererIp gathererPort duration(s) logDirectory");
            System.exit(1);
        }

        String nodeType = args[0];
        int id = Integer.parseInt(args[1]);
        String gaddr = args[2];
        int gport = Integer.parseInt(args[3]);
        long durationSecs = Long.parseLong(args[4]);
        String logDirectory = args[5];

        DataGatherer.configure(durationSecs, logDirectory, gaddr, gport);

        CPUMonitor cm = new CPUMonitor(id, nodeType);

        cm.log();
        cm.saveToFile(new CPUDataPoint());
        cm.sendToGatherer();
    }

    protected void saveToFile(DataPoint dptype) {
        Path logPath = Paths.get(DataGatherer.getLogDirectory(), dptype.getName() + "_" + monitoredName + "_" + monitoredId + ".log");
        try {
            BufferedWriter writer = Files.newBufferedWriter(logPath, StandardCharsets.UTF_8);
            writer.write("# " + dptype.getLogHeader());
            writer.newLine();
            for (DataPoint dp : cpuLog) {
                writer.write(dp.toString());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }


}
