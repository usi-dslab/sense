/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.datapoints.TimelineDataPoint;

public class TimelinePassiveMonitor extends PassiveMonitor {
   
   private TimelineDataPoint currentAverage = null;
   
   public TimelinePassiveMonitor(int monitoredPid, String monitoredName) {
      super(monitoredPid, monitoredName);
   }
   
   public TimelinePassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      super(monitoredPid, monitoredName, recquired);
   }

   // if receive time is not given, calculate it INSIDE the if
   
   synchronized public void logTimeline(Object... timelineTokens) {
      if (active == false) {
         return;
      }
      TimelineDataPoint receivedTimeline = new TimelineDataPoint(1, timelineTokens);
      if (currentAverage == null)
         currentAverage = receivedTimeline;
      else
         currentAverage.add(receivedTimeline);

      long currentTimeMS = System.currentTimeMillis();
      if (currentTimeMS > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
         currentAverage.divideStampsByWeight();
         log.add(currentAverage);
         currentAverage = null;
         lastLogTime = currentTimeMS;
      }
   }

   @Override
   public void saveToFile() {
      saveToFile(new TimelineDataPoint());
   }

   @Override
   public void sendToGatherer() {
      sendToGatherer(new TimelineDataPoint());
   }
}
