/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Long Hoang Le - long.hoang.le@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
public class MemoryDataPoint implements Serializable, DataPoint {

    public long instant;
    public long end;
    // loads refer to the overall load, plus the load of each individual base
    // thus, loads.size() == 1 + system cores
    public ArrayList<MemoryUsage> memoryUsages = new ArrayList<MemoryUsage>();

    public MemoryDataPoint() {
    }

    public MemoryDataPoint(long instant) {
        this.instant = instant;
    }

    public void appendMemoryUsage(long total, long used, long free, long shared, long cache, long available) {
        MemoryUsage cm = new MemoryUsage(total, used, free, shared, cache, available);
        this.memoryUsages.add(cm);
    }

    public void appendMemoryUsage(MemoryUsage load) {
        MemoryUsage cm = new MemoryUsage(load.total, load.used, load.free, load.shared, load.cache, load.available);
        this.memoryUsages.add(cm);
    }

    public void add(MemoryDataPoint other) {
        for (int i = 0; i < other.memoryUsages.size(); i++) {
            MemoryUsage otherMemoryUsage = other.memoryUsages.get(i);
            if (i >= this.memoryUsages.size()) {
                this.appendMemoryUsage(otherMemoryUsage);
            } else {
                MemoryUsage thisMemoryUsage = this.memoryUsages.get(i);
                thisMemoryUsage.add(otherMemoryUsage);
            }
        }
    }

    public void div(double divisor) {
        for (MemoryUsage memoryUsage : memoryUsages) {
            memoryUsage.total /= divisor;
            memoryUsage.used /= divisor;
            memoryUsage.free /= divisor;
            memoryUsage.shared /= divisor;
            memoryUsage.cache /= divisor;
            memoryUsage.available /= divisor;
            memoryUsage.usedPercentage = ((float) memoryUsage.used / memoryUsage.total) * 100;
        }
    }

    @Override
    public long getInstant() {
        return instant;
    }

    @Override
    public long getEnd() {
        return end;
    }

    @Override
    public DataPoint getAverage(List<DataPoint> values) {
        MemoryDataPoint average = new MemoryDataPoint();
        for (DataPoint dcpulog : values) {
            MemoryDataPoint cpuentry = (MemoryDataPoint) dcpulog;
            for (int i = 0; i < cpuentry.memoryUsages.size(); i++) {
                MemoryUsage loadEntry = cpuentry.memoryUsages.get(i);
                if (i >= average.memoryUsages.size()) {
                    average.appendMemoryUsage(loadEntry);
                } else {
                    MemoryUsage avgload = average.memoryUsages.get(i);
                    avgload.add(loadEntry);
                }
            }
        }
        average.div(values.size());
        return average;
    }

    @Override
    public String getName() {
        return "memory";
    }

    @Override
    public String getLogHeader() {
        String header = "memory (usage_percentage total used free shared cache available)";
        return header;
    }

    @Override
    public String toString() {
        String vis = String.format("%d", instant);
        for (MemoryUsage memoryUsage : memoryUsages)
//            vis += String.format(" %.1f %.1f %.1f %.1f", memoryUsage.load, memoryUsage.user, memoryUsage.sys, memoryUsage.idle);
            vis += String.format(" %.1f %d %d %d %d %d %d", memoryUsage.usedPercentage, memoryUsage.total, memoryUsage.used, memoryUsage.free, memoryUsage.shared, memoryUsage.cache, memoryUsage.available);
        return vis;
    }

    @Override
    public DataPoint getAggregate(List<DataPoint> values) {
        MemoryDataPoint aggregate = new MemoryDataPoint();
        for (DataPoint dp : values) {
            MemoryDataPoint cpudp = (MemoryDataPoint) dp;
            aggregate.add(cpudp);
        }
        return aggregate;
    }

    @Override
    public boolean isAggregatable() {
        return true;
    }

    public static class MemoryUsage implements Serializable {

        public long total;
        public long used;
        public long free;
        public long shared;
        public long cache;
        public long available;
        public double usedPercentage;


        public MemoryUsage() {
        }

        public MemoryUsage(long total, long used, long free, long shared, long cache, long available) {
            this.total = total;
            this.used = used;
            this.free = free;
            this.shared = shared;
            this.cache = cache;
            this.available = available;
            this.usedPercentage = ((float) this.used / this.total) * 100;
        }

        public void add(MemoryUsage other) {
            this.total += other.total;
            this.used += other.used;
            this.free += other.free;
            this.shared += other.shared;
            this.cache += other.cache;
            this.available += other.available;
            this.usedPercentage = ((float) this.used / this.total) * 100;
        }

    }
}
