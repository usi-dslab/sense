/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano

 This file is part of Sense.

 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

/**
 * @author Long Hoang Le - long.hoang.le@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.util.List;

public class OracleMoveDataPoint implements DataPoint {
    private static final long serialVersionUID = 1L;

    long begin;
    long end;
    String message;

    public OracleMoveDataPoint() {
    }

    public OracleMoveDataPoint(long begin, long end, String message) {
        this.begin = begin;
        this.end = end;
        this.message = message;
    }

    @Override
    public long getInstant() {
        return end;
    }

    @Override
    public long getEnd() {
        return end;
    }

    @Override
    public DataPoint getAverage(List<DataPoint> values) {
        return null;
    }

    @Override
    public String getName() {
        return "message";
    }

    @Override
    public String getLogHeader() {
        return "messages (moves) ";
    }

    @Override
    public String toString() {
        return String.format("%d %s", getInstant(), message);
    }

    @Override
    public DataPoint getAggregate(List<DataPoint> values) {
        return null;
    }

    @Override
    public boolean isAggregatable() {
        return false;
    }

}
